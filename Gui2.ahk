;************************************************************************************************;
; Author : Rajat Kosh 
; Script Name : GUI2.ahk 
;====================================================================================================================================================================================================
;This File is a part of IDTE
;IDTE- ID3 Tag Editor by Rajat kosh
;Copyright (c) 2013-14 by Team IDTE
;IDTE is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation`, either version 3 of the License`, or (at your option) any later version.
;IDTE is distributed in the hope that it will be useful`, but WITHOUT ANY WARRANTY`; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
;details.You should have received a copy of the GNU General Public License along with IDTE .If not`, see <http://www.gnu.org/licenses/>
;====================================================================================================================================================================================================
if (GUISELECT=2)
{
	XMIN := 0
	YMIN := 0
	YMAX := A_ScreenHeight
	XMAX := A_ScreenWidth
	XTAB := 5
	WTAB := 342
	HTAB := YMAX - 120
	XEDIT := 20
	XYER := 228
	XGEN := 120
	XPRC := 20
	XNXC := 36
	YCOVER := 420
	
	YTOP := 30
	YTOPL := 55
	guicol = F0F0F0
	Gui, Font, S5
	Symbollft := Chr(9664)
	Symbolrit := Chr(9654)
	
	Gui, Add, button , x%XPRC% y618 w17 h17 vprcov gupdwn ,%Symbollft%
	Gui, Add, button , x%XNXC% y618 w17 h17 vnxcov gupupn ,%Symbolrit%
	Gui, Add, Picture, x%XEDIT% y%YCOVER%  vBGpic, %A_WorkingDir%\GUI\CDCASE.png
	Gui, Add, Pic,xp+22 y%YCOVER%  vPic gShowPic
	Gui, Font,
	
	ILA := IL_Create(4, 2, false)
    IL_Add(ILA, "shell32.dll", 259)  ;  Save
    IL_Add(ILA, "shell32.dll", 1) ;  Open File
	IL_Add(ILA, "shell32.dll", 4) ;  Open Folder
    IL_Add(ILA, "shell32.dll", 260) ;  Cut
    IL_Add(ILA, "shell32.dll", 135) ;  Copy
    IL_Add(ILA, "shell32.dll", 261) ;  Paste
    IL_Add(ILA, "shell32.dll", 239) ;  Refresh
    IL_Add(ILA, "shell32.dll", 272) ; Remove
    IL_Add(ILA, "shell32.dll", 270)  ; Quick Mode
	IL_Add(ILA, "shell32.dll", 35) ;  Mini Mode
	IL_Add(ILA, "shell32.dll", 119) ;  EQ
    IL_Add(ILA, "shell32.dll", 155) ;  Help
	
	 ;TBSTYLE_FLAT     := 0x0800 Required to show separators as bars.
	; TBSTYLE_TOOLTIPS := 0x0100 Required to show Tooltips.
	WTOOLBAR:= XMAX/3
	Gui, Add, Custom, x0 y0 h25 w%WTAB% ClassToolbarWindow32 hwndhToolbar 0x0800 0x0100 0x0004 0x0020
	
	IniRead ,lst_Font,%A_MyDocuments%\IDTE_Data\IDTE_Configuration.ini,List_Font,List_Font
	IniRead ,lst_FontSize,%A_MyDocuments%\IDTE_Data\IDTE_Configuration.ini,List_Font,List_FontSize
	IniRead ,lst_Col,%A_MyDocuments%\IDTE_Data\IDTE_Configuration.ini,List_Font,List_Col
	IniRead ,Checker14,%A_MyDocuments%\IDTE_Data\IDTE_Configuration.ini,List_Font,List_Bold
	IniRead ,Checker15,%A_MyDocuments%\IDTE_Data\IDTE_Configuration.ini,List_Font,List_Italic
	IniRead ,Checker16,%A_MyDocuments%\IDTE_Data\IDTE_Configuration.ini,List_Font,List_Underline
	IniRead ,Is_Grid,%A_MyDocuments%\IDTE_Data\IDTE_Configuration.ini,Miscellaneous,Enable_Grid
	Gui, Font, S%lst_FontSize% C%lst_Col%, %lst_Font%
	if(Checker14=1)
		Gui, Font, Bold
	if(Checker15=1)
		Gui, Font, italic
	if(Checker16=1)
		Gui, Font, underline
	WLIST := XMAX - 360
	HLIST := YMAX - 160
	XLIST := WTAB + 10
	Gui, Add, ListView, x%XLIST% y70 w%WLIST% h%HLIST% AltSubmit vMyListView gMyListView %Enable_grid% cBlack hwndHLV LV0x10 LV0x100000, |#|Title|Artist|Album|Year|Genre|Name|In Folder|Band\Orchestra|Publisher|Composer|Comment|Size(MB)|Type|Channels|Sample Rate(Hz)|Bit Rate(Kbps)|Duration|Tags Supported
	LV_ModifyCol(2,"Integer")
	LV_ModifyCol(6,"Integer")
	LV_ModifyCol(14,"Integer")
	LV_ModifyCol(17,"Integer")
	LV_ModifyCol(18,"Integer")
	LV_ModifyCol(19,"Integer")
	Gui,Font,
	margin := 1 
	MaxRange1 :=100
	XVOL := XMAX - 160
	XSEEK := XMAX/2 + 60
	XCOVER := XSEEK - 80
	WSEEK := XMAX/2 - 70
	Gui, Add, Progress,w0 h0 x0 y0 cgray BackgroundSilver vPrBar1  Range0-%MaxRange1%,
	Gui, Add, Progress, w%WSEEK% h10 x%XSEEK% y50 c808080 BackgroundSilver vPrBar2 Range0-%MaxRange1%, 20
	Gui, Add, Progress, x%XVOL% y10 w150 h15 cBlack BackgroundSilver vPrBar3 Range0-%MaxRange1%, 20
	OnMessage(0x201, "WM_LBUTTONDOWN")  ; Monitors Left Clicks on Gui.
	Loop , 3
		GuiControlGet, PrBar%A_Index%, Pos
	Gui, Add, Pic,  x%XCOVER% y5 w60 h60 vMus_ico  ,%A_WorkingDir%\GUI\music_icon40.png
	Gui, Font, s8 cblack bold , Arial
	Gui,add,statusbar, vSbar, 
	SB_SetParts(350,120,100,300,100) ; Make 3 different parts
	XCOVTYPE := XNXC +30
	YCOVTYPE := YCOVER + 200
	Gui ,add ,text, x%XCOVTYPE% y%YCOVTYPE% w200 h15 vcovertype,
	WSTATUS := XCOVER - WTAB -20
	XSTATUS := WTAB + 10
	Gui, Add, text, x%XSTATUS% y5 w%WSTATUS% h55 ReadOnly vClist +Center, Currently No File is Playing
	
	XBTN := XCOVER + 150
	rev_ico=%A_WorkingDir%\GUI\icons\rev.png
	Gui, Add, Pic, x%XBTN% y10 w24 h24 grev vrewind_btn, %rev_ico%
	pre_ico=%A_WorkingDir%\GUI\icons\pre.png
	Gui, Add, Pic, xp+26 yp w24 h24 gBassPrev vpre_btn, %pre_ico%
	play_ico=%A_WorkingDir%\GUI\icons\ply.png
	Gui, Add, Pic, xp+26 yp w24 h24 gPlay vplay_btn, %play_ico%
	pause_ico=%A_WorkingDir%\GUI\icons\pus.png
	Gui, Add, Pic, xp+26 yp w24 h24 gPause vpause_btn, %pause_ico%
	stop_ico=%A_WorkingDir%\GUI\icons\stop.png
	Gui, Add, Pic, xp+26 yp w24 h24 gStop vstop_btn, %stop_ico%
	nxt_ico=%A_WorkingDir%\GUI\icons\nxt.png
	Gui, Add, Pic, xp+26 yp w24 h24 gBassNext vfwd_btn, %nxt_ico%
	fwd_ico=%A_WorkingDir%\GUI\icons\fwd.png
	Gui, Add, Pic, xp+26 yp w24 h24 gfwd vffwd_btn, %fwd_ico%
	
	vol_ico=%A_WorkingDir%\GUI\icons\vol.png
	XVOLBTN := XVOL - 30
	Gui, Add, Pic, x%XVOLBTN% yp-3 w20 h20 gmute vvol_btn, %vol_ico%
	Gui, Add, Text, xp yp+25 w40 h15 vmutetext,
	;Tooltips
	pre_btn_TT := "Previous Track"
	rewind_btn_TT := "Rewind"
	play_btn_TT := "Play"
	pause_btn_TT := "Pause"
	stop_btn_TT := "Stop"
	ffwd_btn_TT := "Forward"
	fwd_btn_TT := "Next Track"
	
	SB_SetText("Welcome To IDTE",4)
	Gui, Add, Text, x%XSEEK% y30 w40 h15 vInfo ,
	XINFO2 := XSEEK + WSEEK - 50
	Gui, Add, Text, x%XINFO2% y30 w40 h15 vInfo2,
	XGRAPHIC := XSEEK+25
	Gui, Add, Progress, x%XGRAPHIC% y20 w5 h10 AltSubmit cBlack Background%guicol% vertical vRIT,	
	Gui, Add, Progress, xp-5 y15 w5 h15 AltSubmit cBlack Background%guicol% vertical vRIGT,
	Gui, Add, Progress, xp-5 y10 w5 h20 AltSubmit cBlack Background%guicol% vertical vRIGHT,	
	Gui, Add, Progress, xp-5 y10 w5 h20 AltSubmit cBlack Background%guicol% vertical vLEFT, 
	Gui, Add, Progress, xp-5 y15 w5 h15 AltSubmit cBlack Background%guicol% vertical vLET, 
	Gui, Add, Progress, xp-5 y20 w5 h10 AltSubmit cBlack Background%guicol% vertical vLT,
	Gui, Font,
	Gui, Add, Button, Hidden Default, OK
	GuiControl,, Pic , *w199 *h195 %A_WorkingDir%\GUI\CD Inside.png
	LV_ModifyCol(1, "Integer") ; For sorting, indicate that the Size column is an integer.
	LV_ModifyCol(9, "Integer") ; For sorting, indicate that the Size column is an integer.
; Create an ImageList so that the ListView can display some icons:
	ImageListID1 := IL_Create(10)
	ImageListID2 := IL_Create(10, 10, true)  ; A list of large icons to go with the small ones.
	YEDIT := YTOP+45 
	YLABEL := YTOP+25
	Gui , Add,Tab2, x%XTAB% y%YTOP% w%WTAB% h%HTAB% vTabs, Tag Panel|Lyrics And More|Player Mode
	Gui, Add, Edit, x%XEDIT% y%YEDIT% w300 h20 vArtist, Artist
	Gui, Add, Edit, x%XEDIT% yp+45 w300 h20 vAlbum, Album
	Gui, Add, Edit, x%XEDIT% yp+45 w300 h20 vTitle, Title
	Gui, Add, Edit, x%XEDIT% yp+45 w90 h20 vTrack, Track
	Gui, Add, Edit, x%XYER% yp w90 h20 vYear, Year
	Gui, Add, Edit, x%XGEN% yp w100 h20 vGenre, Genre
	Gui, Add, Edit, x%XEDIT% yp+45 w300 h20 vComposer, Composer
	Gui, Add, Edit, x%XEDIT% yp+45 w300 h20 vComments, Comments
	Gui, Add, Edit, x%XEDIT% yp+45 w300 h20 vPublisher, Publisher
	Gui, Add, Edit, x%XEDIT% yp+45 w300 h20 vBand, Album Artist
	IniRead ,tg_Font,%A_MyDocuments%\IDTE_Data\IDTE_Configuration.ini,Tag_Panel,Tag_Font
	IniRead ,tg_FontSize,%A_MyDocuments%\IDTE_Data\IDTE_Configuration.ini,Tag_Panel,Tag_FontSize
	IniRead ,tg_Col,%A_MyDocuments%\IDTE_Data\IDTE_Configuration.ini,Tag_Panel,Tag_Col
	IniRead ,Checker20,%A_MyDocuments%\IDTE_Data\IDTE_Configuration.ini,Tag_Panel,Tag_Bold
	IniRead ,Checker21,%A_MyDocuments%\IDTE_Data\IDTE_Configuration.ini,Tag_Panel,Tag_Italic
	IniRead ,Checker22,%A_MyDocuments%\IDTE_Data\IDTE_Configuration.ini,Tag_Panel,Tag_Underline 
	Gui, Font, S%tg_FontSize% C%tg_Col%, %tg_Font%
	if(Checker20=1)
		Gui, Font, Bold
	if(Checker21=1)
		Gui, Font, italic
	if(Checker22=1)
		Gui, Font, underline
	Gui, Add, Text, x%XEDIT% y%YLABEL% w130 h15 vlabel1, Artist
	Gui, Add, Text, x%XEDIT% yp+45 w130 h15 vlabel2, Album
	Gui, Add, Text, x%XEDIT% yp+45 w130 h15 vlabel3, Title
	Gui, Add, Text, x%XEDIT% yp+45 w80 h15 vlabel4, Track
	Gui, Add, Text, x%XYER% yp w80 h15 vlabel5, Year
	Gui, Add, Text, x%XGEN% yp w80 h15 vlabel6, Genre
	Gui, Add, Text, x%XEDIT% yp+45 w130 h15 vlabel7, Composer
	Gui, Add, Text, x%XEDIT% yp+45 w80 h15 vlabel8, Comments
	Gui, Add, Text, x%XEDIT% yp+45 w130 h15 vlabel9, Publisher	
	Gui, Add, Text, x%XEDIT% yp+45  h15 vlabel10, Album Artist
	Gui , Tab , Lyrics And More
	Gui, Font, S8 CDefault bold, Arial
	XGBX := XEDIT - 10
	XTXT := XMIN - 218
	Gui, Add, GroupBox , x%XGBX% y155 h70 w315 ,Add Extended Field
	Gui, Add, Text, x%XGBX% y225 , Lyrics
	Gui, Add, Edit , x%XGBX% y245 w315 h115 vLRC , Lyrics
	Gui, Add, Text, x%XGBX% y30, Detailed Information	
	Gui, Add, Edit , x%XGBX% y50 w315 h100 vSong_info readonly, No Information Available
	Gui, Add, Text, x%XEDIT% y175 h15, Field Name 
	Gui, Add, Edit, x%XEDIT% y190 wp+30 vFname,
	Gui, Add, Text, x%XTXT% y175 h15, Field Text
	Gui, Add, Edit, x%XTXT% y190 w130 vFtext,
	XADD := XMIN - 76
	XFT := XMIN - 278
	XOPT := XMIN - 237
	XFART := XMIN - 208
	Gui,Add, Button, x%XADD% y185 , Add
	Gui, Add, Button , x%XEDIT% y365 , Done
	Gui,Add,Button, x%XFT% y365,Fetch 
	Gui,Add,Button, x%XOPT% y365 w20 vLRCOPT, >>
	Gui,Add,Button, x%XFART% y365 vfa,&Fetch Cover Art
	
	XCNT := XMIN - 300
	XSL := XMIN - 210
	XFST := XMIN - 160
	XCST := XMIN - 110
	Gui , Tab , Player Mode
	Gui, Font, S8 CDefault bold italic, Comic Sans MS
	Gui, Add, Text , x%XGBX% y30 vText_l3, Auto Scroll Lyrics
	Gui, Add, pic, x%XGBX% y50 w315 h260 vPict gAll hwndVZLN
	GuiControl , hide , Pict
	Gui, Add, Edit , x%XGBX% y50 w315 h260 +Center hwndGUI vLRC2 ReadOnly,
	Gui,Add,Button, x%XGBX% y320 vstp_but ,Stop
	Gui,Add,Button, x%XCNT% y320  vcnt_but ,Continue
	Gui,Add,Button, x%XSL% y320  vslw_but ,Slow
	Gui,Add,Button, x%XFST% y320  vfst_but ,Fast
	Gui,Add,Button, x%XCST% y320  vcus_but,Custom
	XVIS := XMIN-50
	Gui,Add,Pic,	x%XVIS% y320 w24 h24 vvis_tgl gVisual_Toggle , %A_WorkingDir%\GUI\icons\visual.bmp
	GuiControl , hide , Pict
	Gui, Add, Text, x%XGBX% y365 w300 vScroll_Rate
	
	Done_TT:= "Save Lyrics To Selected File(s)"
	Fetch_TT:= "Fetch Lyrics From Internet"
	Add_TT:="Add This Field Into Tag"
	Gui, +Resize
	Gui, Show , w1360 h695 Maximize, IDTE-ID`3 Tag Editor
	Gui, +LastFound
	
	OnMessage(0x200, "WM_MOUSEMOVE")
	OnMessage(0x205, "WM_MOUSELEFT")
	OnMessage(0x204, "WM_MOUSELEFTART")
	
	DllCall("ChangeWindowMessageFilter", uint, 0x49, uint, 1)
	DllCall("ChangeWindowMessageFilter", uint, 0x233, uint, 1)
		
	; Set a function to monitor the Toolbar's messages.
	WM_COMMAND := 0x111
	OnMessage(WM_COMMAND, "TB_Messages")

	; Set a function to monitor notifications.
	WM_NOTIFY := 0x4E
	OnMessage(WM_NOTIFY, "TB_Notify")

	MyToolbar := New Toolbar(hToolbar)
	; Set ImageList.
	MyToolbar.SetImageList(ILA)
	
	
	; Add buttons.
	MyToolbar.Add("", "", "ContextSave=Save:1", "ContextFile=Open File:2", "ContextFold=Open Folder:3",
        , "", "CutTag=Cut:4", "CopyTag=Copy:5", "PasteTag=Paste:6"
        , "", "RefreshList=Refesh List:7", "ContextClearRows=Remove From List:8"
        , "", "FastMode=Quick Tag Mode:9", "MiniMode=Mini Mode:10", "Equaliz=Equalizer:11", "", "helpTag=Help Contents:12")
	
	; Removes text labels and show them as tooltips.
	MyToolbar.SetMaxTextRows(0)
	MyToolbar.AutoSize()
}